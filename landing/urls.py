from django.contrib import admin
from django.urls import path
from .views import index,submit_form,out
urlpatterns = [
    path('', index,name="index"),
    path('output/',out,name='output'),
    path('submit/',submit_form,name='submit_form')
]
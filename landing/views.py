from django.shortcuts import render,redirect
from .forms import StatusForm
from .models import Status
# Create your views here.
def index(request):
    response={'status_form':StatusForm}
    return render (request,'landing.html',response)
def submit_form(request):
    if (request.method=="POST"):
        form=StatusForm(request.POST)
        if (form.is_valid()):
            status=Status(status=form.cleaned_data['status'])
            status.save()
    return redirect('/')
def out(request):
    response={}
    response['output']=Status.objects.all()
    return render(request,'submit.html',response)